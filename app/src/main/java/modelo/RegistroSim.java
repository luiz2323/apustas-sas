package modelo;

public class RegistroSim {

    @Override
    public String toString() {
        return "numero:" + numeroSim + '\n' +
                "codigo:" + codigoSim + '\n' +
                "clave:" + claveSIm + '\n' +
                "Descripcion:" + Descripcion + '\n' +
                "fecha Corte:" + fechaCorte + '\n';
    }

    private String numeroSim;
    private String codigoSim;
    private String claveSIm;
    private String Descripcion;
    private String fechaCorte;
    public RegistroSim() {
    }

    public RegistroSim(String numeroSim, String codigoSim, String claveSIm, String descripcion, String fechaCorte) {
        this.numeroSim = numeroSim;
        this.codigoSim = codigoSim;
        this.claveSIm = claveSIm;
        Descripcion = descripcion;
        this.fechaCorte = fechaCorte;

    }



    public String getNumeroSim() {
        return numeroSim;
    }

    public void setNumeroSim(String numeroSim) {
        this.numeroSim = numeroSim;
    }

    public String getCodigoSim() {
        return codigoSim;
    }

    public void setCodigoSim(String codigoSim) {
        this.codigoSim = codigoSim;
    }

    public String getClaveSIm() {
        return claveSIm;
    }

    public void setClaveSIm(String claveSIm) {
        this.claveSIm = claveSIm;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getFechaCorte() {
        return fechaCorte;
    }

    public void setFechaCorte(String fechaCorte) {
        this.fechaCorte = fechaCorte;
    }
}
