package modelo;

public class RegistroEstado {
    @Override
    public String toString() {
        return  "numero:" + numero + '\n' +
                "Estado:" + Estado + '\n' +
                "fecha Instalacion:" + fechaInstala + '\n' +
                "puntoVenta:" + puntoVenta + '\n' +
                "personalInstalador:" + personalInstalador;
    }

    private String numero;
    private String Estado;
    private String fechaInstala;
    private String puntoVenta;
    private String personalInstalador;


    public RegistroEstado() {
    }

    public RegistroEstado(String numero, String estado, String fechaInstala, String puntoVenta, String personalInstalador) {
        this.numero = numero;
        Estado = estado;
        this.fechaInstala = fechaInstala;
        this.puntoVenta = puntoVenta;
        this.personalInstalador = personalInstalador;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFechaInstala() {
        return fechaInstala;
    }

    public void setFechaInstala(String fechaInstala) {
        this.fechaInstala = fechaInstala;
    }

    public String getPuntoVenta() {
        return puntoVenta;
    }

    public void setPuntoVenta(String puntoVenta) {
        this.puntoVenta = puntoVenta;
    }

    public String getPersonalInstalador() {
        return personalInstalador;
    }

    public void setPersonalInstalador(String personalInstalador) {
        this.personalInstalador = personalInstalador;
    }
}
