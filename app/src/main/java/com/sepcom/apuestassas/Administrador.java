package com.sepcom.apuestassas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.time.Year;
import java.util.Calendar;
import java.util.Random;

import modelo.RegistroSim;

public class Administrador extends AppCompatActivity {
    private Firebase firebase;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private EditText TXTnumeroSIm;
    private EditText TXTcodigoSIm;
    private EditText TXTClaveSim;
    private EditText TXTDescripcion;
    private Button TXTfechaCorte;
    private Button btnEnviarRegSim;
    private int ano;
    private int mes;
    private int dia;

    private DatePickerDialog.OnDateSetListener calendarioEsc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrador);
        inicializar();
    }

    private void inicializar() {
        Firebase.setAndroidContext(this);
        firebaseDatabase =FirebaseDatabase.getInstance();
        firebase = new Firebase("https://apuestassas.firebaseio.com/");
        databaseReference = firebaseDatabase.getReference();


        TXTnumeroSIm = findViewById(R.id.TXTnumeroSIm);
        TXTcodigoSIm = findViewById(R.id.TXTcodigoSIm);
        TXTClaveSim = findViewById(R.id.TXTClaveSim);
        TXTDescripcion = findViewById(R.id.TXTDescripcion);
        TXTfechaCorte = findViewById(R.id.TXTfechaCorte);


        Calendar calendario = Calendar.getInstance();
        ano = calendario.get(Calendar.YEAR);
        mes = calendario.get(Calendar.MONTH);
        dia = calendario.get(Calendar.DAY_OF_MONTH);

        calendarioEsc = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                ano = year;
                mes = month;
                dia = dayOfMonth;
                TXTfechaCorte.setText(ano+"/"+mes+"/"+dia);
            }
        };



        TXTfechaCorte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);

            }
        });
        btnEnviarRegSim = findViewById(R.id.btnEnviarRegsim);



        btnEnviarRegSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresarFom();
            }
        });


    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id){
            case 0:
                return new DatePickerDialog(this,calendarioEsc,ano,mes,dia);


            default:
                break;
        }
        return null;
    }

    private void ingresarFom() {
        String numerosim = TXTnumeroSIm.getText().toString();
        String codigosim = TXTcodigoSIm.getText().toString();
        String clavesim = TXTClaveSim.getText().toString();
        String descripcion = TXTDescripcion.getText().toString();
        String fechacorte = TXTfechaCorte.getText().toString();
        RegistroSim registroSim = new RegistroSim();
        registroSim.setNumeroSim(numerosim);
        registroSim.setCodigoSim(codigosim);
        registroSim.setClaveSIm(clavesim);
        registroSim.setDescripcion(descripcion);
        registroSim.setFechaCorte(fechacorte);
        firebase.child("RegistroSim").child(String.valueOf(registroSim.getClaveSIm())).setValue(registroSim);
        clear();
        Toast.makeText(this, "La informacion se ingreso correctamente ", Toast.LENGTH_SHORT).show();

        if (TextUtils.isEmpty(numerosim) || TextUtils.isEmpty(codigosim) || TextUtils.isEmpty(clavesim) || TextUtils.isEmpty(descripcion) || TextUtils.isEmpty(fechacorte)) {
            Toast.makeText(this, "No puede haber ningun campo sin información", Toast.LENGTH_LONG).show();

        }else{

        }
    }

    private void clear() {
        TXTnumeroSIm.setText("");
        TXTcodigoSIm.setText("");
        TXTClaveSim.setText("");
        TXTDescripcion.setText("");
        TXTfechaCorte.setText("seleccionar fecha");
    }
}