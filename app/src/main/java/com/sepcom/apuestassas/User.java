package com.sepcom.apuestassas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;

import modelo.RegistroSim;

public class User extends AppCompatActivity {
    private EditText buscador;
    private Button btnBuscar , btnestado;
    private ListView lista;
    private ArrayList<RegistroSim> list;
    private DatabaseReference ref;
    private ArrayList<RegistroSim>milist;
    private ArrayAdapter<RegistroSim> arrayAdaptersim ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        inicializar();
    }

    private void inicializar() {
        buscador = findViewById(R.id.buscador);
        btnBuscar = findViewById(R.id.btnBuscar);
        milist = new ArrayList<>();
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                milist.clear();
                String numero = buscador.getText().toString();
                if (!TextUtils.isEmpty(numero)){
                for (RegistroSim obj  : list) {
                    if (obj.getNumeroSim().contains(numero)){
                        milist.add(obj);

                    }
                }
                }
                arrayAdaptersim = new ArrayAdapter<RegistroSim>(User.this,android.R.layout.simple_list_item_1,milist);
                lista.setAdapter(arrayAdaptersim);

            }
        });
        btnestado = findViewById(R.id.btnEstado);
        btnestado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (milist.size()>0){
                    Intent intent = new Intent(User.this,Estado.class);
                    intent.putExtra("numero",milist.get(0).getNumeroSim());
                    intent.putExtra("codigo",milist.get(0).getCodigoSim());
                    intent.putExtra("clave",milist.get(0).getClaveSIm());
                    intent.putExtra("descripcion",milist.get(0).getDescripcion());
                    intent.putExtra("fechaCorte",milist.get(0).getFechaCorte());
                    startActivity(intent);


                }else {
                    Toast.makeText(User.this, "Primero debes buscar un registro", Toast.LENGTH_SHORT).show();
                }
            }
        });
        lista = findViewById(R.id.listaT);
        ref = FirebaseDatabase.getInstance().getReference().child("RegistroSim");
        list = new ArrayList<>();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();

                if (dataSnapshot.exists()){
                    for (DataSnapshot snapshop: dataSnapshot.getChildren()) {
                        RegistroSim sim =snapshop.getValue(RegistroSim.class);
                        list.add(sim);
                        
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
}