package com.sepcom.apuestassas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;

import modelo.RegistroEstado;
import modelo.RegistroSim;

public class Estado extends AppCompatActivity {
    private EditText estado;
    private EditText puntoVenta;
    private EditText PersonalInstalacion;
    private Button fechainsta;
    private Button btningresarEstado;
    private ListView vistaLista;
    private ArrayList<String> list ;
    private ArrayAdapter<String> arrayAdapter;

    private int ano;
    private int mes;
    private int dia;

    private DatePickerDialog.OnDateSetListener calendarioEsc;


    private Firebase firebase;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado);
        inicializar();


    }

    private void inicializar() {
        Firebase.setAndroidContext(this);
        firebaseDatabase =FirebaseDatabase.getInstance();
        firebase = new Firebase("https://apuestassas.firebaseio.com/");
        databaseReference = firebaseDatabase.getReference();

        estado = findViewById(R.id.Estado);
        puntoVenta = findViewById(R.id.Puntoventa);
        PersonalInstalacion = findViewById(R.id.PersonalInstalacion);
        fechainsta = findViewById(R.id.btnFechaInsta);
        fechainsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);
            }
        });
        btningresarEstado = findViewById(R.id.btnInsertarEstado);
        btningresarEstado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });
        Calendar calendario = Calendar.getInstance();
        ano = calendario.get(Calendar.YEAR);
        mes = calendario.get(Calendar.MONTH);
        dia = calendario.get(Calendar.DAY_OF_MONTH);

        calendarioEsc = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                ano = year;
                mes = month;
                dia = dayOfMonth;
                fechainsta.setText(ano+"/"+mes+"/"+dia);
            }
        };
        vistaLista = findViewById(R.id.listaEstado);
        list = new ArrayList<>();
        list.add(getIntent().getStringExtra("numero"));
        list.add(getIntent().getStringExtra("codigo"));
        list.add(getIntent().getStringExtra("clave"));
        list.add(getIntent().getStringExtra("descripcion"));
        list.add(getIntent().getStringExtra("fechaCorte"));
        arrayAdapter = new ArrayAdapter<String>(Estado.this,android.R.layout.simple_list_item_1,list);
        vistaLista.setAdapter(arrayAdapter);






    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id){
            case 0:
                return new DatePickerDialog(this,calendarioEsc,ano,mes,dia);


            default:
                break;
        }
        return null;
    }

    private void ingresar() {
        String estado_in = estado.getText().toString();
        String puntoVenta_in = puntoVenta.getText().toString();
        String fechainsta_in = fechainsta.getText().toString();
        String personal = PersonalInstalacion.getText().toString();

        RegistroEstado estado = new RegistroEstado();
        estado.setEstado(estado_in);
        estado.setPuntoVenta(puntoVenta_in);
        estado.setFechaInstala(fechainsta_in);
        estado.setPersonalInstalador(personal);
        estado.setNumero(getIntent().getStringExtra("numero"));
        firebase.child("Estado").child(getIntent().getStringExtra("numero")).setValue(estado);

        Toast.makeText(this, "La informacion se ingreso correctamente ", Toast.LENGTH_SHORT).show();

        if (TextUtils.isEmpty(estado_in) || TextUtils.isEmpty(puntoVenta_in) || TextUtils.isEmpty(fechainsta_in) || TextUtils.isEmpty(personal)) {
            Toast.makeText(this, "No puede haber ningun campo sin información", Toast.LENGTH_LONG).show();

        }else{

        }
        retornar();
    }

    private void retornar() {
        finish();
    }
}