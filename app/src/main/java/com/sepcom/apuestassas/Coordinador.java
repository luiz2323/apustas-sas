package com.sepcom.apuestassas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import modelo.RegistroEstado;
import modelo.RegistroSim;

public class Coordinador extends AppCompatActivity {
    private EditText buscador;
    private ListView lista;
    private Button btnbuscador;
    private ArrayList<RegistroEstado> list;
    private DatabaseReference ref;
    private ArrayList<RegistroEstado>milist;
    private ArrayAdapter<RegistroEstado> arrayAdaptersim ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinador);
        inicializar();
    }


    private void inicializar() {
        buscador = findViewById(R.id.buscadorCoordinador);
        btnbuscador = findViewById(R.id.btnBuscarCoordinador);
        milist = new ArrayList<>();
        btnbuscador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                milist.clear();
                String numero = buscador.getText().toString();
                if (!TextUtils.isEmpty(numero)){
                    for (RegistroEstado obj  : list) {
                        if (obj.getNumero().contains(numero) || obj.getEstado().contains(numero) || obj.getPersonalInstalador().contains(numero)){
                            milist.add(obj);

                        }
                    }
                }
                arrayAdaptersim = new ArrayAdapter<RegistroEstado>(Coordinador.this,android.R.layout.simple_list_item_1,milist);
                lista.setAdapter(arrayAdaptersim);

            }
        });

        lista = findViewById(R.id.listaTCoordinador);
        ref = FirebaseDatabase.getInstance().getReference().child("Estado");
        list = new ArrayList<>();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();

                if (dataSnapshot.exists()){
                    for (DataSnapshot snapshop: dataSnapshot.getChildren()) {
                        RegistroEstado sim =snapshop.getValue(RegistroEstado.class);
                        list.add(sim);


                    }
                }
                arrayAdaptersim = new ArrayAdapter<RegistroEstado>(Coordinador.this,android.R.layout.simple_expandable_list_item_1,list);
                lista.setAdapter(arrayAdaptersim);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
}