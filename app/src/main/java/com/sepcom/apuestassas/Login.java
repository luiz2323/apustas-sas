package com.sepcom.apuestassas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
    private EditText usuario;
    private EditText password;
    private Button ingresar;
    private FirebaseAuth auth;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inicializar();
    }

    private void inicializar() {
        usuario = findViewById(R.id.email);
        password = findViewById(R.id.password);
        ingresar = findViewById(R.id.btnEnviar);
        auth = FirebaseAuth.getInstance();
        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();

            }
        });
    }

    private void login() {
        final String email = usuario.getText().toString();
        String pass = password.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)){
            Toast.makeText(this, "No puede haber ningun campo sin información", Toast.LENGTH_LONG).show();
        }else{
            auth.signInWithEmailAndPassword(email,pass)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                switch (usuario.getText().toString()){
                                    case "admin@gmail.com":
                                        Toast.makeText(Login.this, "Bienvenido "+usuario.getText().toString(), Toast.LENGTH_SHORT).show();
                                        intent = new Intent(Login.this, Administrador.class);
                                        startActivity(intent);
                                        finish();
                                        break;
                                    case "user@gmail.com":
                                        Toast.makeText(Login.this, "Bienvenido "+usuario.getText().toString(), Toast.LENGTH_SHORT).show();
                                        intent = new Intent(Login.this, User.class);
                                        startActivity(intent);
                                        finish();
                                        break;
                                    case "coordinador@gmail.com":
                                        Toast.makeText(Login.this, "Bienvenido "+usuario.getText().toString(), Toast.LENGTH_SHORT).show();
                                        intent = new Intent(Login.this, Coordinador.class);
                                        startActivity(intent);
                                        finish();

                                }
                            }else{
                                Toast.makeText(Login.this, "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }
}