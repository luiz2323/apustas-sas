package constantes;

import com.google.firebase.database.FirebaseDatabase;

public class FirebasePersistencia extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
